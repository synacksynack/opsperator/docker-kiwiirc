#!/bin/sh

if test "$DEBUG"; then
    set -x
    DO_LOGLEVEL=1
else
    DO_LOGLEVEL=2
fi
. /usr/local/bin/nsswrapper.sh

if ! test -s /etc/kiwiirc/config.conf; then
    INSTANCE_NAME="${INSTANCE_NAME:-KubeIRC}"
    IRC_UPSTREAM_HOST=${IRC_UPSTREAM_HOST:-irc.freenode.net}
    IRC_UPSTREAM_PASSWORD="${IRC_UPSTREAM_PASSWORD:-}"
    IRC_UPSTREAM_PORT=${IRC_UPSTREAM_PORT:-6667}
    IRC_UPSTREAM_TIMEOUT=${IRC_UPSTREAM_TIMEOUT:-5}
    IRC_UPSTREAM_TLS=${IRC_UPSTREAM_TLS:-false}
    JWT_SECRET="${JWT_SECRET:-}"

    sed -e "s|LOGLEVEL|$DO_LOGLEVEL|" \
	-e "s|INSTANCE|$INSTANCE_NAME|" \
	-e "s|IRCPASS|$IRC_UPSTREAM_PASSWORD|" \
	-e "s|IRCPORT|$IRC_UPSTREAM_PORT|" \
	-e "s|IRCSERVER|$IRC_UPSTREAM_HOST|" \
	-e "s|IRCTIMEOUT|$IRC_UPSTREAM_TIMEOUT|" \
	-e "s|IRCTLS|$IRC_UPSTREAM_TLS|" \
	-e "s|JWTSECRET|$JWT_SECRET|" \
	/config.conf >/etc/kiwiirc/config.conf
fi
if ! test -s /etc/kiwiirc/client.json; then
    sed -e "s|INSTANCE|$INSTANCE_NAME|" \
	/client.json >/etc/kiwiirc/client.json
fi

unset INSTANCE_NAME DO_LOGLEVEL JWT_SECRET IRC_UPSTREAM_HOST IRC_UPSTREAM_PORT \
    IRC_UPSTREAM_TLS IRC_UPSTREAM_TIMEOUT IRC_UPSTREAM_PASSWORD

exec kiwiirc --config /etc/kiwiirc/config.conf
