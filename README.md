# k8 KiwiIRC

KiwiIRC image.

Build with:

```
$ make build
```

Start Demo in OpenShift:

```
$ make ocdemo
```

Cleanup OpenShift assets:

```
$ make ocpurge
```


Environment variables and volumes
----------------------------------

The image recognizes the following environment variables that you can set during
initialization by passing `-e VAR=VALUE` to the Docker `run` command.

|    Variable name        |    Description                  | Default                 |
| :---------------------- | ------------------------------- | ----------------------- |
| `INSTANCE_NAME`         | Kiwi IRC Instance Name          | `KubeIRC`               |
| `IRC_UPSTREAM_HOST`     | IRC Upstream Server             | `irc.freenode.net`      |
| `IRC_UPSTREAM_PASSWORD` | IRC Upstream Password           | undef                   |
| `IRC_UPSTREAM_PORT`     | IRC Upstream Port               | `6667`                  |
| `IRC_UPSTREAM_TIMEOUT`  | IRC Upstream Timeout            | `5` seconds             |
| `IRC_UPSTREAM_TLS`      | IRC Upstream uses TLS           | `false`                 |
| `JWT_SECRET`            | Kiwi IRC JWT Secret             | undef                   |

You can also set the following mount points by passing the `-v /host:/container`
flag to Docker.

|  Volume mount point  | Description                     |
| :------------------- | ------------------------------- |
|  `/etc/kiwiirc`      | Kiwi IRC Site Configuration     |
