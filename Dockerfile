FROM opsperator/apache

ARG DO_UPGRADE=
# Kiwi IRC image for OpenShift Origin

ENV KW_ROOTURL=https://kiwiirc.com/downloads \
    KW_VERSION=20.05.24.1

LABEL io.k8s.description="Kiwi IRC $KW_VERSION - Web IRC." \
      io.k8s.display-name="Kiwi IRC $KW_VERSION" \
      io.openshift.expose-services="8080:http" \
      io.openshift.tags="kiwiirc,webirc" \
      io.openshift.non-scalable="true" \
      help="For more information visit https://gitlab.com/synacksynack/opsperator/docker-kiwiirc" \
      maintainer="Samuel MARTIN MORO <faust64@gmail.com>" \
      version="$KW_VERSION"

USER root

COPY config/* /

RUN set -x \
    && apt-get update \
    && if test "$DO_UPGRADE"; then \
	echo "# Upgrade Base Image"; \
	apt-get -y upgrade; \
	apt-get -y dist-upgrade; \
    fi \
    && echo "# Install Kiwi IRC" \
    && apt-get install -y wget unzip \
    && if test `uname -m` = aarch64; then \
	export PKGARCH=arm64; \
    else \
	export PKGARCH=amd64; \
    fi \
    && wget "$KW_ROOTURL/kiwiirc_${KW_VERSION}_linux_$PKGARCH.zip" \
	-O /usr/src/kiwiirc.zip \
    && ( \
	cd /usr/src \
	&& unzip kiwiirc.zip; \
    ) \
    && mv /usr/src/kiwiirc_linux_$PKGARCH/www /usr/share/kiwiirc \
    && mv /usr/src/kiwiirc_linux_$PKGARCH/kiwiirc /usr/bin/kiwiirc \
    && echo "# Fixing permissions" \
    && mkdir -p /etc/kiwiirc \
    && chown -R 1001:root /etc/kiwiirc \
    && chmod -R g=u /etc/kiwiirc \
    && chown -R root:root /usr/bin/kiwiirc /usr/share/kiwiirc \
    && chmod 0755 /usr/bin/kiwiirc \
    && echo "# Cleaning Up" \
    && apt-get -y remove --purge wget unzip \
    && apt-get autoremove -y --purge \
    && apt-get clean \
    && rm -rvf /usr/src/kiwiirc.zip /usr/src/kiwiirc_linux_$PKGARCH \
    && rm -rvf /usr/share/man /usr/share/doc /var/lib/apt/lists/* \
    && unset PKGARCH HTTP_PROXY HTTPS_PROXY NO_PROXY DO_UPGRADE http_proxy \
	https_proxy

ENTRYPOINT [ "dumb-init", "--", "/run-kiwiirc.sh" ]
USER 1001
